#################################################
#
#  GMP_07 another branch for 'Kimo-Kawaii' character
#  Modified by Shigeto 'Shige' Maeda  2015-9-21
#  s.maeda@oud-japan.co.jp
#
#################################################

from bpy import *
import bpy, bmesh
import math, mathutils, random, sys
from xml.etree import ElementTree
import xml.etree.ElementTree as NF

# set the PATH to the [samples] folder
filePath = '/Users/shige/desktop/samples/'

class GMP_Panel(bpy.types.Panel):
	bl_label = "GMP 7"
	bl_space_type = "VIEW_3D"
	bl_region_type = "TOOLS"
	
	def draw(self, context):
		layout = self.layout
		
		scn = bpy.context.scene
		
		row = layout.row()
		
#		row = row.column()
#		row.label(text="Seed System")
		
		row = row.column()
		row.label(text=" ")
		
		row = row.column()
		row.prop( scn, "objName" )
		
		row = row.column()
		row.prop( scn, "numberOfVariation")
		
		row = row.column()
		row.prop( scn, "singleColor")
		
		row = row.column()
		row.label(text=" ")
		
		row.operator( "bpt.generate_op" )

class GMP_Operator(bpy.types.Operator):
	bl_idname = "bpt.generate_op"
	bl_label = "Generate"
 
	def execute(self, context):
#----------------------------------------------------------------- Main Code to here

		objFile = bpy.context.scene.objName
#		filePath = '/Users/shige/desktop/samples/'
		singleColor = bpy.context.scene.singleColor
		numberOfVariation = bpy.context.scene.numberOfVariation + 1
		
		#read .xml file for parameters
#		XMLFILE = filePath + objFile + ".dat"
#		tree = ElementTree.parse(XMLFILE)
#		root = tree.getroot()

#		To avoid error about materials that is used now!
		for i in range(20):
			bpy.context.scene.layers[i] = True

		deleteAllObjects()
		deleteMaterials()

		mFactList = []

		#hKeisu = [0.0, -0.79, -0.43, -0.12, 0.18, 0.29, 0.41, 0.73, 0.97]
		hKeisu = [0.0, -0.23, -0.11, -0.05, -0.02, 0.13, 0.21, 0.37, 0.43]
		hTanni = [1.0, -1.0, 0.3, -1.0, 5.0, 3.0, 1.0, 1.0, 2.0, 2.0, 10.0]

		for i in range(numberOfVariation):
			mFact = []
			for j in range(11):
				mFact.append(hKeisu[i] * hTanni[j])
			mFactList.append(mFact)	
	
		print(mFactList)

		for i in range(numberOfVariation):
			mFact = mFactList[i]
			generate(i, mFact, objFile, singleColor)
		
		selectAllObjects()	
		return {'FINISHED'}
		
def register():
	bpy.utils.register_class( GMP_Operator )
	bpy.utils.register_class( GMP_Panel )
 
def unregister():
	bpy.utils.register_class( GMP_Operator )
	bpy.utils.register_class( GMP_Panel )

#------------------------------------------------------------------------------------ Main Code from here

#selectAllObjects
def selectAllObjects():
	bpy.ops.object.select_all(action='SELECT')

# deleteAllObjects()
def deleteAllObjects():
	bpy.ops.object.select_all(action='SELECT')
	bpy.ops.object.delete(use_global=False)

# addNewPlane
def addNewPlane():
	bpy.ops.mesh.primitive_plane_add(radius=1, view_align=False, enter_editmode=False, location=(0, 0, 0), layers=(False, False, False, False, False, False, False, False, True, False, False, False, False, False, False, False, False, False, False, False))
	bpy.ops.transform.resize(value=(0.1, 0.1, 0.1), constraint_axis=(False, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1)
	
# editMode
def editMode():
	bpy.ops.object.mode_set(mode='EDIT')
	
# objectMode
def objectMode():
	bpy.ops.object.mode_set(mode='OBJECT')

# selectAll
def selectAll():
	bpy.ops.mesh.select_all(action='SELECT')
	
# deselectAll()
def deselectAll():
	bpy.ops.mesh.select_all(action='DESELECT')
	
# selectMode2Face
def selectMode2Face():
	bpy.ops.mesh.select_mode(type='FACE', action='TOGGLE')

# ext_Face(dt)
def ext_Face(dt):
	bpy.ops.mesh.extrude_faces_move(MESH_OT_extrude_faces_indiv={"mirror":False}, TRANSFORM_OT_shrink_fatten={"value":-dt, "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "release_confirm":False})

# rot_FaceX(degree)
def rot_FaceX(angle):
	bpy.ops.transform.rotate(value=(math.radians(angle)), axis=(-1, -2.22045e-16, -0), constraint_axis=(False, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1)
	
# scale_Face(size)
def scale_Face(size):
	bpy.ops.transform.resize(value=(size, size, size), constraint_axis=(False, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1)

# fillFace()
def fillFace():
	bpy.ops.mesh.edge_face_add()

# delHalf
def delHalf():
	editMode()
	deselectAll()
	objectMode()
	ob = bpy.context.object
	me = ob.data
	for p in me.polygons:
		if p.center[0] < 0.0:
			p.select = True
	editMode()
	bpy.ops.mesh.delete(type='FACE')
	objectMode()
	
# addSubSurf(level)
def addSubSurf(level):
	bpy.ops.object.mode_set(mode='OBJECT')
	bpy.ops.object.modifier_add(type='SUBSURF')
	object = bpy.context.active_object
	object.modifiers['Subsurf'].levels = level

# applySubSurf()
def applySubSurf():
	bpy.ops.object.modifier_apply(apply_as='DATA', modifier="Subsurf")

# addMirror()
def addMirror():
	bpy.ops.object.modifier_add(type='MIRROR')

# makeMaterials(num)
def makeMaterials(num):
	for i in range(num + 1):
		matNum = i	
		matName = 'material' + str(matNum)

		mat = bpy.data.materials.new(matName)
		if i == 0:
			mat.diffuse_color = (1.0, 1.0, 1.0)
		else:
			mat.diffuse_color = (random.random(), random.random(), random.random())
			
		mat.texture_slots.add()

		obj = bpy.context.scene.objects.active
		obj.data.materials.append(mat)

# deleteMaterials()
def deleteMaterials():
	for item in bpy.data.materials:
		ob = bpy.context.object
		bpy.data.materials.remove(item, do_unlink = True)
	
# selectFaces(param)
def selectFaces(param):
	angleFrom = param[0]
	angleTo = param[1]
	placeXFrom = param[2]
	placeXTo = param[3]
	placeYFrom = param[4]
	placeYTo = param[5]
	placeZFrom = param[6]
	placeZTo = param[7]
	perc = param[8]
	materialNum = param[9]
	area_limit = param[10]
	targetMaterial = param[11]
	vGroup = 'part1'
	ob = bpy.context.object
	me = ob.data
	newGroup = ob.vertex_groups.new(vGroup)
	objectMode()
	
	up = mathutils.Vector((0.0,0.0,1.0))

	# Lets loop through all the faces in the mesh
	for p in me.polygons:
		# If the angle between up and the face's normal (direction) is smaller than 45... 
		# The face must be pointing up
		# To compare the angle we need 45 degrees in radians, not in degrees!
		# Math with angles is usually all in radians
		if p.normal.angle(up) > math.radians(angleFrom) and p.normal.angle(up) < math.radians(angleTo) and p.center[0] > placeXFrom and p.center[0] < placeXTo and p.center[1] > placeYFrom and p.center[1] < placeYTo and p.center[2] > placeZFrom and p.center[2] < placeZTo and p.area > area_limit and random.random() < perc and (targetMaterial == 99 or targetMaterial == p.material_index) :
			# Set select to true
			p.select = True
			p.material_index = materialNum
		else:
			p.select = False
			
	editMode()
	
	bpy.ops.object.vertex_group_set_active(group=vGroup)
	bpy.ops.object.vertex_group_assign()
	bpy.ops.mesh.select_all(action='DESELECT')

# selMode2FA()
def selMode2FA():
	ob = bpy.context.object
	editMode()
	bpy.ops.mesh.select_mode(type='FACE')
	objectMode()

# selMat(matNum)
def selMat(materialNum):
	ob = bpy.context.object
	editMode()
	bpy.context.object.active_material_index = materialNum
	bpy.ops.object.material_slot_select()
	objectMode()

# extrudeOneStep(oneStep)
def extrudeOneStep(oneStep):
	dZ = oneStep[0] #Kyori
	sX = oneStep[1] #Scale-X
	sY = oneStep[2] #Scale-Y
	rA = oneStep[3] #RotatonAngle
	aX = oneStep[4] #Axis-X
	aY = oneStep[5] #Axis-Y
	aZ = oneStep[6] #Axis-Z
	
	objectMode()
	
	selMode2FA()
	me = bpy.context.object.data
	# Scale control by face(area) size-->
	context = bpy.context
	if (context.active_object != None):
		object = bpy.context.active_object
	
	# Loop through the faces to find the center
	rotDir = 1	
				
#	px = 1
#	py = 1
#	pz = 1
	
	editMode()
	
	bpy.ops.transform.shrink_fatten(value=dZ * sX, mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1.0, snap=False, snap_target='CLOSEST', snap_point=(0.0, 0.0, 0.0), snap_align=False, snap_normal=(0.0, 0.0, 0.0), release_confirm=False)
	bpy.ops.transform.resize(value=(sX, sY, sX), constraint_axis=(True, True, True), constraint_orientation='NORMAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=0.0762767, snap=False, snap_target='CLOSEST', snap_point=(0, 0, 0), snap_align=False, snap_normal=(0, 0, 0), texture_space=False, release_confirm=False)
	bpy.ops.transform.rotate(value=rA*rotDir, axis=(aX, aY, aZ), constraint_axis=(True, True, True), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1, release_confirm=True)	
	bpy.ops.object.vertex_group_remove_from(use_all_groups=True)
	
#	bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1) # <-------------------- coment out for speed up

# multiExtrude()
def multiExtrude(mltData):
	materialNum = mltData[0] 
	mainLevel = mltData[1]
	stepNum = mltData[2] 
	stepDist = mltData[3] 
	stepRot = mltData[4] 
	startDeg = mltData[5] 
	stepDeg = mltData[6] 
	modLevel = mltData[7] 
	modRate = mltData[8]
	axis = mltData[9]
	jointNum = mltData[10]
	newMaterialNum = mltData[11]
	rndRate = mltData[12]
	
	ob = bpy.context.object
	me = ob.data

	deselectAll()
	editMode()
	selMat(materialNum)
		
	# Go into object mode so that we are sure we have the current list of faces 
	objectMode()
	# Make a nice list to keep the vertex groups in
	groupList = []

	# Now loop through all the faces
	for i, f in enumerate(ob.data.polygons):
		
		# If this face is selected
		if ob.data.polygons[i].select == True and ob.data.polygons[i].area > 0.07:

			# Create a new vertex group!
			newGroup = ob.vertex_groups.new('mygroup')
			groupList.append(newGroup)

			# Get all the vertices in the current face and add them to the new group
			for v in f.vertices:
				newGroup.add([v], 1.0, 'REPLACE')
			
	# Now we loop through the groups and do what we want.
	for g in groupList:

		# Make sure nothing is selected
		editMode()
		deselectAll()
		# Make the group in our list the active one
		ob.vertex_groups.active_index = g.index

		# Select all the verts in the active group
		bpy.ops.object.vertex_group_select()

		bpy.ops.object.vertex_group_remove(all=False)	

		# AND NOW WE CAN DO OUR STUFF... little test example follows
		t = startDeg
		oldValue = 1.0
		oldValue2 = 1.0
		rndVal = random.random() * rndRate
		# Shape
		j = 0
		for i in range(stepNum):
     
			bpy.ops.mesh.extrude_region_move(MESH_OT_extrude_region={"mirror":False}, TRANSFORM_OT_translate={"value":(0, 0, 0), "constraint_axis":(False, False, True), "constraint_orientation":'NORMAL', "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "texture_space":False, "remove_on_cancel":False, "release_confirm":False})
			
			# An added trick... remove the extruded face from all vertex groups after the first extrusion (i == 0)
			# This way it can't get extruded again
			# Because the edge of the first face can be part of multiple groups
			if not i:
				bpy.ops.object.vertex_group_remove_from(use_all_groups=True)

			editMode()
			
			newValue = (math.sin(math.radians(t)+math.sin(math.radians(t/1.13)))+2.0)*mainLevel  #*2
			newValue2 = (math.sin(math.radians(t*0.5)+math.sin(math.radians(t/0.97)))+2.0)*mainLevel
			newRatio = newValue/oldValue
			newRatio2 = newValue2/oldValue2
			objectMode()
			# Get the mesh
			me = ob.data
			# Loop through the faces to find the center
			area = 1
			for f in me.polygons:
				if f.select == True:
					area = math.sqrt(f.area) * 3
			editMode()
			dA = stepDist * area
			if i == 0:
				dA *= 0.3
			sA = newRatio
			sA2 = newRatio2
			rA = modLevel*math.sin(math.radians(t*modRate))*(-stepRot/10) + rndVal
#			rA = math.sin(t*0.3) * 0.5
			axis_x = axis[0]
			axis_y = axis[1]
			axis_z = axis[2]
			axis_z = 0.5
			#rZ = math.sin(math.radians(i*45 + bodyNum+45))*0.5 #<---------------------------------------
			rZ = 0
				
			morphData = [dA, sA, sA, rA, axis_x, axis_y, axis_z,rZ]
			extrudeOneStep(morphData)
			
			if jointNum != 0:
				j = j+1
				if j == jointNum:
					bpy.ops.mesh.extrude_region()
					oneStep = [ -0.01, 0.6, 0.6, 0, axis_x, axis_y, axis_z ]
					extrudeOneStep(oneStep) # <-------------0
					bpy.ops.mesh.extrude_region()
					oneStep = [ -0.01, 1.66, 1.66, 0, axis_x, axis_y, axis_z ]
					extrudeOneStep(oneStep) # <-------------0
					j = 0
			t = t + stepDeg
			oldValue = newValue
			oldValue2 = newValue2
			
		if materialNum != newMaterialNum:
			objectMode()
			for p in me.polygons:
				if p.select:
					p.material_index = newMaterialNum
	bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1) # <-------------------- coment out for speed up	
	editMode()

# makeBallEyes()
def makeBallEyes(eyeInfo):
	eyeballsColor = eyeInfo[0]
	irisColor = eyeInfo[1]
	firstStep = eyeInfo[2]
	eyeSharpness1 = eyeInfo[3]
	eyeSharpness2 = eyeInfo[4]
	eyeSharpness3 = eyeInfo[5]
	materialNum = eyeInfo[6]

	ob = bpy.context.object
	me = ob.data

	deselectAll()
	editMode()
	selMat(materialNum)
		
	# Go into object mode so that we are sure we have the current list of faces 
	objectMode()
	# Make a nice list to keep the vertex groups in
	groupList = []

	# Now loop through all the faces
	for i, f in enumerate(me.polygons):
		
		# If this face is selected
		if me.polygons[i].select == True:

			# Create a new vertex group!
			newGroup = ob.vertex_groups.new('mygroup')
			groupList.append(newGroup)

			# Get all the vertices in the current face and add them to the new group
			for v in f.vertices:
				newGroup.add([v], 1.0, 'REPLACE')
			
	# Now we loop through the groups and do what we want.
	for g in groupList:

		# Make sure nothing is selected
		editMode()
		deselectAll()
		# Make the group in our list the active one
		ob.vertex_groups.active_index = g.index

		# Select all the verts in the active group
		bpy.ops.object.vertex_group_select()

		objectMode()
		me = bpy.context.object.data
		for p in me.polygons:
			if p.select == True:
				keisu = math.sqrt(p.area) / 10.0
		editMode()
		
		bpy.ops.object.vertex_group_remove(all=False)	

		# AND NOW WE CAN DO OUR STUFF... little test example follows
		
		dFactor = [ 0.0, -0.2, 0.0, 0.0, -5.0/keisu, -10.0/keisu, -10.0/(keisu * eyeSharpness3), 0.0, 0.0, 0.0, 0.5 ]
		mFactor = [ 0.6, 1.0, 0.8, 1.0, eyeSharpness1, eyeSharpness2, eyeSharpness3, 0.5, 0.9, 1.0, 1.0 ]
		# Shape the eyes 1
		for i in range(0, 3):
			bpy.ops.mesh.extrude_region_move(MESH_OT_extrude_region={"mirror":False}, TRANSFORM_OT_translate={"value":(0, 0, 0), "constraint_axis":(False, False, True), "constraint_orientation":'NORMAL', "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "texture_space":False, "remove_on_cancel":False, "release_confirm":False})
#			bpy.ops.mesh.extrude_faces_move(MESH_OT_extrude_faces_indiv={"mirror":False}, TRANSFORM_OT_shrink_fatten={"value":0, "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "release_confirm":False})
			bpy.ops.mesh.looptools_circle(custom_radius=False, fit='best', flatten=True, influence=100, lock_x=False, lock_y=False, lock_z=False, radius=1, regular=True)

			if not i:
				bpy.ops.object.vertex_group_remove_from(use_all_groups=True)

			dA = dFactor[ i ] * keisu
			sA = mFactor[ i ]
			sA2 = mFactor[ i ]
			rA = 0.1
			axis_x = 0
			axis_y = 1
			axis_z = 0
			rZ = 0
				
			morphData = [dA*2, sA, sA, rA, axis_x, axis_y, axis_z, rZ]
			extrudeOneStep(morphData)
		# Shape the eyes 2
		bpy.context.object.active_material_index = eyeballsColor
		bpy.ops.object.material_slot_assign()
		
		for i in range(3, 7):
			bpy.ops.mesh.extrude_faces_move(MESH_OT_extrude_faces_indiv={"mirror":False}, TRANSFORM_OT_shrink_fatten={"value":0, "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "release_confirm":False})

			if not i:
				bpy.ops.object.vertex_group_remove_from(use_all_groups=True)

			dA = dFactor[ i ] * keisu
			sA = mFactor[ i ]
			sA2 = mFactor[ i ]
			rA = 0.1
			axis_x = 0
			axis_y = 1
			axis_z = 0
			rZ = 0
				
			morphData = [dA*2, sA, sA, rA, axis_x, axis_y, axis_z, rZ]
			extrudeOneStep(morphData)
		# Shape the eyes 3	
		bpy.context.object.active_material_index = irisColor
		bpy.ops.object.material_slot_assign()
		for i in range(7, 8):
			bpy.ops.mesh.extrude_faces_move(MESH_OT_extrude_faces_indiv={"mirror":False}, TRANSFORM_OT_shrink_fatten={"value":0, "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "release_confirm":False})

			if not i:
				bpy.ops.object.vertex_group_remove_from(use_all_groups=True)

			dA = dFactor[ i ] * keisu
			sA = mFactor[ i ]
			sA2 = mFactor[ i ]
			rA = 0.1
			axis_x = 0
			axis_y = 1
			axis_z = 0
			rZ = 0
				
			morphData = [dA*2, sA, sA, rA, axis_x, axis_y, axis_z, rZ]
			extrudeOneStep(morphData)
			
	editMode()
	
# makeLongEyes()
def makeLongEyes(eyeInfo):
	eyeballsColor = eyeInfo[0]
	irisColor = eyeInfo[1]
	firstStep = eyeInfo[2]
	eyeSharpness1 = eyeInfo[3]
	eyeSharpness2 = eyeInfo[4]
	eyeSharpness3 = eyeInfo[5]
	materialNum = eyeInfo[6]

	ob = bpy.context.object
	me = ob.data

	deselectAll()
	editMode()
	selMat(materialNum)
		
	# Go into object mode so that we are sure we have the current list of faces 
	objectMode()
	# Make a nice list to keep the vertex groups in
	groupList = []

	# Now loop through all the faces
	for i, f in enumerate(me.polygons):
		
		# If this face is selected
		if me.polygons[i].select == True:

			# Create a new vertex group!
			newGroup = ob.vertex_groups.new('mygroup')
			groupList.append(newGroup)

			# Get all the vertices in the current face and add them to the new group
			for v in f.vertices:
				newGroup.add([v], 1.0, 'REPLACE')
			
	# Now we loop through the groups and do what we want.
	for g in groupList:

		# Make sure nothing is selected
		editMode()
		deselectAll()
		# Make the group in our list the active one
		ob.vertex_groups.active_index = g.index

		# Select all the verts in the active group
		bpy.ops.object.vertex_group_select()

		objectMode()
		me = bpy.context.object.data
		for p in me.polygons:
			if p.select == True:
				keisu = math.sqrt(p.area) / 10.0
		editMode()
		
		bpy.ops.object.vertex_group_remove(all=False)	

		# AND NOW WE CAN DO OUR STUFF... little test example follows
		
		dFactor = [ -2.0, -2.0, 0.0, -2.0, -5.0/keisu, -10.0/keisu, -10.0/(keisu * eyeSharpness3), 0.0, -1.0, -1.0, 0.5 ]
		mFactor = [ 0.6, 0.7, 0.8, 1.0, eyeSharpness1, eyeSharpness2, eyeSharpness3, 0.7, 0.9, 0.6, 1.0 ]
		# Shape the eyes 1
		for i in range(0, 3):
			bpy.ops.mesh.extrude_region_move(MESH_OT_extrude_region={"mirror":False}, TRANSFORM_OT_translate={"value":(0, 0, 0), "constraint_axis":(False, False, True), "constraint_orientation":'NORMAL', "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "texture_space":False, "remove_on_cancel":False, "release_confirm":False})
#			bpy.ops.mesh.extrude_faces_move(MESH_OT_extrude_faces_indiv={"mirror":False}, TRANSFORM_OT_shrink_fatten={"value":0, "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "release_confirm":False})

			if not i:
				bpy.ops.object.vertex_group_remove_from(use_all_groups=True)

			dA = dFactor[ i ] * keisu
			sA = mFactor[ i ]
			sA2 = mFactor[ i ]
			rA = 0.1
			axis_x = 0
			axis_y = 1
			axis_z = 0
			rZ = 0
				
			morphData = [dA*2, sA, sA, rA, axis_x, axis_y, axis_z, rZ]
			extrudeOneStep(morphData)
		# Shape the eyes 2
		bpy.context.object.active_material_index = eyeballsColor
		bpy.ops.object.material_slot_assign()
		
		for i in range(3, 7):
			bpy.ops.mesh.extrude_faces_move(MESH_OT_extrude_faces_indiv={"mirror":False}, TRANSFORM_OT_shrink_fatten={"value":0, "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "release_confirm":False})

			if not i:
				bpy.ops.object.vertex_group_remove_from(use_all_groups=True)

			dA = dFactor[ i ] * keisu
			sA = mFactor[ i ]
			sA2 = mFactor[ i ]
			rA = 0.1
			axis_x = 0
			axis_y = 1
			axis_z = 0
			rZ = 0
				
			morphData = [dA*2, sA, sA, rA, axis_x, axis_y, axis_z, rZ]
			extrudeOneStep(morphData)
		# Shape the eyes 3	
		bpy.context.object.active_material_index = irisColor
		bpy.ops.object.material_slot_assign()
		for i in range(7, 9):
			bpy.ops.mesh.extrude_faces_move(MESH_OT_extrude_faces_indiv={"mirror":False}, TRANSFORM_OT_shrink_fatten={"value":0, "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "release_confirm":False})

			if not i:
				bpy.ops.object.vertex_group_remove_from(use_all_groups=True)

			dA = dFactor[ i ] * keisu
			sA = mFactor[ i ]
			sA2 = mFactor[ i ]
			rA = 0.1
			axis_x = 0
			axis_y = 1
			axis_z = 0
			rZ = 0
				
			morphData = [dA*2, sA, sA, rA, axis_x, axis_y, axis_z, rZ]
			extrudeOneStep(morphData)
			
	editMode()
	
# makeHoleEyes()
def makeHoleEyes(eyeInfo):
	eyeballsColor = eyeInfo[0]
	irisColor = eyeInfo[1]
	firstStep = eyeInfo[2]
	eyeSharpness1 = eyeInfo[3]
	eyeSharpness2 = eyeInfo[4]
	eyeSharpness3 = eyeInfo[5]
	materialNum = eyeInfo[6]

	ob = bpy.context.object
	me = ob.data

	deselectAll()
	editMode()
	selMat(materialNum)
		
	# Go into object mode so that we are sure we have the current list of faces 
	objectMode()
	# Make a nice list to keep the vertex groups in
	groupList = []

	# Now loop through all the faces
	for i, f in enumerate(me.polygons):
		
		# If this face is selected
		if me.polygons[i].select == True:

			# Create a new vertex group!
			newGroup = ob.vertex_groups.new('mygroup')
			groupList.append(newGroup)

			# Get all the vertices in the current face and add them to the new group
			for v in f.vertices:
				newGroup.add([v], 1.0, 'REPLACE')
			
	# Now we loop through the groups and do what we want.
	for g in groupList:

		# Make sure nothing is selected
		editMode()
		deselectAll()
		# Make the group in our list the active one
		ob.vertex_groups.active_index = g.index

		# Select all the verts in the active group
		bpy.ops.object.vertex_group_select()

		objectMode()
		me = bpy.context.object.data
		for p in me.polygons:
			if p.select == True:
				keisu = math.sqrt(p.area) / 10.0
		editMode()
		
		bpy.ops.object.vertex_group_remove(all=False)	

		# AND NOW WE CAN DO OUR STUFF... little test example follows
		
		dFactor = [ 0.0, -0.2, 0.0, 0.0, -keisu/12.0, -keisu/5.0, -keisu / (eyeSharpness3*5.0), 0.0, 0.0, 0.0, 0.5 ]
		mFactor = [ 0.6, 1.0, 0.8, 1.0, eyeSharpness1, eyeSharpness2, eyeSharpness3, 0.5, 0.9, 1.0, 1.0 ]
		# Shape the eyes 1
		for i in range(0, 3):
			bpy.ops.mesh.extrude_region_move(MESH_OT_extrude_region={"mirror":False}, TRANSFORM_OT_translate={"value":(0, 0, 0), "constraint_axis":(False, False, True), "constraint_orientation":'NORMAL', "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "texture_space":False, "remove_on_cancel":False, "release_confirm":False})
#			bpy.ops.mesh.extrude_faces_move(MESH_OT_extrude_faces_indiv={"mirror":False}, TRANSFORM_OT_shrink_fatten={"value":0, "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "release_confirm":False})
			bpy.ops.mesh.looptools_circle(custom_radius=False, fit='best', flatten=True, influence=100, lock_x=False, lock_y=False, lock_z=False, radius=1, regular=True)

			if not i:
				bpy.ops.object.vertex_group_remove_from(use_all_groups=True)

			dA = dFactor[ i ] * keisu
			sA = mFactor[ i ]
			sA2 = mFactor[ i ]
			rA = 0.1
			axis_x = 0
			axis_y = 1
			axis_z = 0
			rZ = 0
				
			morphData = [dA*2, sA, sA, rA, axis_x, axis_y, axis_z, rZ]
			extrudeOneStep(morphData)
		# Shape the eyes 2
		bpy.context.object.active_material_index = eyeballsColor
		bpy.ops.object.material_slot_assign()
		
		for i in range(3, 8):
			bpy.ops.mesh.extrude_faces_move(MESH_OT_extrude_faces_indiv={"mirror":False}, TRANSFORM_OT_shrink_fatten={"value":0, "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "release_confirm":False})

			if not i:
				bpy.ops.object.vertex_group_remove_from(use_all_groups=True)

			dA = dFactor[ i ] * keisu
			sA = mFactor[ i ]
			sA2 = mFactor[ i ]
			rA = 0.1
			axis_x = 0
			axis_y = 1
			axis_z = 0
			rZ = 0
				
			morphData = [dA*2, sA, sA, rA, axis_x, axis_y, axis_z, rZ]
			extrudeOneStep(morphData)
		# Shape the eyes 3	
		bpy.context.object.active_material_index = irisColor
		bpy.ops.object.material_slot_assign()
		for i in range(8, 11):
			bpy.ops.mesh.extrude_faces_move(MESH_OT_extrude_faces_indiv={"mirror":False}, TRANSFORM_OT_shrink_fatten={"value":0, "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "release_confirm":False})

			if not i:
				bpy.ops.object.vertex_group_remove_from(use_all_groups=True)

			dA = dFactor[ i ] * keisu
			sA = mFactor[ i ]
			sA2 = mFactor[ i ]
			rA = 0.1
			axis_x = 0
			axis_y = 1
			axis_z = 0
			rZ = 0
				
			morphData = [dA*2, sA, sA, rA, axis_x, axis_y, axis_z, rZ]
			extrudeOneStep(morphData)
			
	editMode()
		
# makeOhmIrisEyes()
def makeOhmIrisEyes(eyeInfo):
	eyeballsColor = eyeInfo[0]
	irisColor = eyeInfo[1]
	firstStep = eyeInfo[2]
	eyeSharpness1 = eyeInfo[3]
	eyeSharpness2 = eyeInfo[4]
	eyeSharpness3 = eyeInfo[5]
	materialNum = eyeInfo[6]
	
	ob = bpy.context.object
	me = ob.data

	deselectAll()
	editMode()
	selMat(materialNum)
		
	# Go into object mode so that we are sure we have the current list of faces 
	objectMode()
	# Make a nice list to keep the vertex groups in
	groupList = []

	# Now loop through all the faces
	for i, f in enumerate(me.polygons):
		
		# If this face is selected
		if me.polygons[i].select == True:

			# Create a new vertex group!
			newGroup = ob.vertex_groups.new('mygroup')
			groupList.append(newGroup)

			# Get all the vertices in the current face and add them to the new group
			for v in f.vertices:
				newGroup.add([v], 1.0, 'REPLACE')
			
	# Now we loop through the groups and do what we want.
	for g in groupList:

		# Make sure nothing is selected
		editMode()
		deselectAll()
		# Make the group in our list the active one
		ob.vertex_groups.active_index = g.index

		# Select all the verts in the active group
		bpy.ops.object.vertex_group_select()
		keisu = 1.0
		objectMode()
		me = bpy.context.object.data
		for p in me.polygons:
			if p.select == True:
				keisu = math.sqrt(p.area) / 8.0
		editMode()
		
		bpy.ops.object.vertex_group_remove(all=False)	

		# AND NOW WE CAN DO OUR STUFF... little test example follows
		dFactor = [ 0.0, -0.2, 0.0, 0.0, -eyeSharpness1/1.5, -eyeSharpness1/2 ]
		mFactor = [ firstStep, 1.0, 0.8, 1.0, eyeSharpness1, eyeSharpness2 ]
		# Shape the eyes 1
		for i in range(0, 4):
			bpy.ops.mesh.extrude_region_move(MESH_OT_extrude_region={"mirror":False}, TRANSFORM_OT_translate={"value":(0, 0, 0), "constraint_axis":(False, False, True), "constraint_orientation":'NORMAL', "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "texture_space":False, "remove_on_cancel":False, "release_confirm":False})
#			bpy.ops.mesh.extrude_faces_move(MESH_OT_extrude_faces_indiv={"mirror":False}, TRANSFORM_OT_shrink_fatten={"value":0, "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "release_confirm":False})
			bpy.ops.mesh.looptools_circle(custom_radius=False, fit='best', flatten=True, influence=100, lock_x=False, lock_y=False, lock_z=False, radius=1, regular=True)
			
			if not i:
				bpy.ops.object.vertex_group_remove_from(use_all_groups=True)

			dA = dFactor[ i ] * keisu
			sA = mFactor[ i ]
			sA2 = mFactor[ i ]
			rA = 0.1
			axis_x = 0
			axis_y = 1
			axis_z = 0
			rZ = 0
				
			morphData = [dA*2, sA, sA, rA, axis_x, axis_y, axis_z, rZ]
			extrudeOneStep(morphData)
		# Shape the eyes 2
		bpy.context.object.active_material_index = eyeballsColor
		bpy.ops.object.material_slot_assign()
		for i in range(4, 6):
			bpy.ops.mesh.extrude_faces_move(MESH_OT_extrude_faces_indiv={"mirror":False}, TRANSFORM_OT_shrink_fatten={"value":0, "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "release_confirm":False})

			if not i:
				bpy.ops.object.vertex_group_remove_from(use_all_groups=True)

			dA = dFactor[ i ] * keisu
			sA = mFactor[ i ]
			sA2 = mFactor[ i ]
			rA = 0.1
			axis_x = 0
			axis_y = 1
			axis_z = 0
			rZ = 0
				
#			morphData = [dA*2, sA, sA, rA, axis_x, axis_y, axis_z, rZ]
#			extrudeOneStep(morphData)
#			bpy.context.object.active_material_index = irisColor
#			bpy.ops.object.material_slot_assign()
				
			morphData = [dA*2, sA, sA, rA, axis_x, axis_y, axis_z, rZ]
			extrudeOneStep(morphData)
			
		bpy.context.object.active_material_index = irisColor
		bpy.ops.object.material_slot_assign()
		
	editMode()	
	
# makeFins()
def makeFins():
	ob = bpy.context.object
	me = ob.data

	deselectAll()
	editMode()
	selMat(materialNum)
		
	# Go into object mode so that we are sure we have the current list of faces 
	objectMode()
	# Make a nice list to keep the vertex groups in
	groupList = []

	# Now loop through all the faces
	for i, f in enumerate(me.polygons):
		
		# If this face is selected
		if me.polygons[i].select == True:

			# Create a new vertex group!
			newGroup = ob.vertex_groups.new('mygroup')
			groupList.append(newGroup)

			# Get all the vertices in the current face and add them to the new group
			for v in f.vertices:
				newGroup.add([v], 1.0, 'REPLACE')
			
	# Now we loop through the groups and do what we want.
	for g in groupList:

		# Make sure nothing is selected
		editMode()
		deselectAll()
		# Make the group in our list the active one
		ob.vertex_groups.active_index = g.index

		# Select all the verts in the active group
		bpy.ops.object.vertex_group_select()

		bpy.ops.object.vertex_group_remove(all=False)	

		# AND NOW WE CAN DO OUR STUFF... little test example follows
		# Shape the fins
		dFactor = [ -1.5, -1.5, -4.0, -4.0, -2.0 ]
		mxFactor = [ 0.5, 1.0, 1.0, 1.0, 0.5 ]
		myFactor = [ 0.5, 3.0, 0.9, 0.5, 0.5 ]
		rFactor = [ -50.0, 0.0, 50.0, 0.0, 0.0 ]
		for i in range(5):
			
			bpy.ops.mesh.extrude_region_move(MESH_OT_extrude_region={"mirror":False}, TRANSFORM_OT_translate={"value":(0, 0, 0), "constraint_axis":(False, False, True), "constraint_orientation":'NORMAL', "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "texture_space":False, "remove_on_cancel":False, "release_confirm":False})
			
			if not i:
				bpy.ops.object.vertex_group_remove_from(use_all_groups=True)

			dA = dFactor[ i ]
			sA = mxFactor[ i ]
			sA2 = myFactor[ i ]
			rA = rFactor[ i ]
			axis_x = 0
			axis_y = 1
			axis_z = 0
			rZ = 0
				
			morphData = [dA, sA, sA2, rA, axis_x, axis_y, axis_z, rZ]
			extrudeOneStep(morphData)
			
	editMode()

#make mouth
def makeMouth():
	editMode()
	
	#select p.material_index = 0
	bpy.context.object.active_material_index = 0
	bpy.ops.object.material_slot_select()
	bpy.context.object.active_material_index = mouthColor
	bpy.ops.object.material_slot_assign()
#	bpy.ops.mesh.looptools_circle(custom_radius=False, fit='best', flatten=True, influence=100, lock_x=False, lock_y=False, lock_z=False, radius=1, regular=True)
	reSize = 4.0
	bpy.ops.transform.resize(value=(reSize, reSize, reSize), constraint_axis=(False, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=2.85312)
	bpy.ops.mesh.extrude_faces_move(MESH_OT_extrude_faces_indiv={"mirror":False}, TRANSFORM_OT_shrink_fatten={"value":0, "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "release_confirm":False})
#	bpy.ops.mesh.extrude_region_shrink_fatten(MESH_OT_extrude_region={"mirror":False}, TRANSFORM_OT_shrink_fatten={"value":-0.0, "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":2.85312, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "release_confirm":False})
	reSize = 0.95
	bpy.ops.transform.resize(value=(reSize, reSize, reSize), constraint_axis=(False, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=2.85312)
	extDist = 6.0
	bpy.ops.mesh.extrude_region_shrink_fatten(MESH_OT_extrude_region={"mirror":False}, TRANSFORM_OT_shrink_fatten={"value":extDist, "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":2.85312, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "release_confirm":False})
	bpy.ops.mesh.vertices_smooth(repeat=3)

# randomBodyColor(bodyMatNum)
def randomBodyColor(bodyMatNum, bodyColor, div, materials):
	#get the current body color
	bpy.context.object.active_material_index = bodyColor
	baseColor = bpy.context.object.active_material.diffuse_color
	#make new body colors
	for i in range(bodyMatNum):
		matNum = i + 1
		matName = 'bodyColor' + str(matNum)
		mat = bpy.data.materials.new(matName)
		randFactor = random.random()
		mat.diffuse_color = (baseColor[0] * randFactor, baseColor[1] * randFactor, baseColor[2] * randFactor)
#		print(mat.diffuse_color)
		
		mat.texture_slots.add()
		obj = bpy.context.scene.objects.active
		obj.data.materials.append(mat)
	
	objectMode()
	me = bpy.context.object.data
	for p in me.polygons:
#		if p.material_index == bodyColor:
		if p.material_index == bodyColor or p.material_index < (div+8):
#			p.material_index = materials + 1 + (p.index % 5)
			p.material_index = materials + 1 + random.randint(0, bodyMatNum - 1)
						
################################################################################################################################## main program
def generate(genNum, mFact, objFile, singleColor):

	#read .xml file for parameters
	XMLFILE = filePath + objFile + ".dat"
	tree = ElementTree.parse(XMLFILE)
	root = tree.getroot()

	#completeAndBodycolor
	basicParam = [int(root.find('makeSeparate').text), int(root.find('bodyColorNum').text)]
	#headParam
	headParam = [int(root.find('materials').text), float(root.find('baseDist').text), int(root.find('totalStep').text), float(root.find('freqA').text), float(root.find('phaseA').text), float(root.find('freqB').text), float(root.find('phaseB').text), float(root.find('amplitude').text), float(root.find('faceRot').text), int(root.find('divNum').text),  int(root.find('spinePart').text), int(root.find('legPart').text), int(root.find('armPart').text), int(root.find('eyesPart').text)]
	#bodyParam
	bodyParam = [float(root.find('bodyPhase').text), float(root.find('bodyMod').text), float(root.find('bodyAmp').text), float(root.find('bodyAngleFrom').text), float(root.find('bodyAngleTo').text), float(root.find('bodyLocXFrom').text), float(root.find('bodyLocXTo').text), float(root.find('bodyLocYFrom').text), float(root.find('bodyLocYTo').text), float(root.find('bodyLocZFrom').text), float(root.find('bodyLocZTo').text), float(root.find('bodyPerc').text), float(root.find('bodyArea_limit').text), int(root.find('bLength').text), float(root.find('bWidthFreq').text), float(root.find('bPhase').text), float(root.find('bAmp').text), float(root.find('bDist').text), float(root.find('bodyRFreq').text), float(root.find('bodyRPhase').text), float(root.find('bodyRDeg').text)]
	#legParam
	legParam = [float(root.find('legAngleFrom').text), float(root.find('legAngleTo').text), float(root.find('legLocXFrom').text), float(root.find('legLocXTo').text), float(root.find('legLocYFrom').text), float(root.find('legLocYTo').text), float(root.find('legLocZFrom').text), float(root.find('legLocZTo').text), float(root.find('legPerc').text), float(root.find('legArea_limit').text), float(root.find('legMainLevel').text), int(root.find('legStepNum').text), float(root.find('legStepDist').text), float(root.find('legStepRot').text), float(root.find('legStartDeg').text), float(root.find('legStepDeg').text), float(root.find('legModLevel').text), float(root.find('legModRate').text), float(root.find('legAxisX').text), float(root.find('legAxisY').text), float(root.find('legAxisZ').text), int(root.find('legJointNum').text), float(root.find('legRndRate').text)]
	#clawParam
	clawParam = [float(root.find('clawAngleFrom').text), float(root.find('clawAngleTo').text), float(root.find('clawLocXFrom').text), float(root.find('clawLocXTo').text), float(root.find('clawLocYFrom').text), float(root.find('clawLocYTo').text), float(root.find('clawLocZFrom').text), float(root.find('clawLocZTo').text), float(root.find('clawPerc').text), float(root.find('clawArea_limit').text), float(root.find('clawMainLevel').text), int(root.find('clawStepNum').text), float(root.find('clawStepDist').text), float(root.find('clawStepRot').text), float(root.find('clawStartDeg').text), float(root.find('clawStepDeg').text), float(root.find('clawModLevel').text), float(root.find('clawModRate').text), float(root.find('clawAxisX').text), float(root.find('clawAxisY').text), float(root.find('clawAxisZ').text), int(root.find('clawJointNum').text), float(root.find('clawRndRate').text)]
	#armParam
	armParam = [float(root.find('armAngleFrom').text), float(root.find('armAngleTo').text), float(root.find('armLocXFrom').text), float(root.find('armLocXTo').text), float(root.find('armLocYFrom').text), float(root.find('armLocYTo').text), float(root.find('armLocZFrom').text), float(root.find('armLocZTo').text), float(root.find('armPerc').text), float(root.find('armArea_limit').text), float(root.find('armMainLevel').text), int(root.find('armStepNum').text), float(root.find('armStepDist').text), float(root.find('armStepRot').text), float(root.find('armStartDeg').text), float(root.find('armStepDeg').text), float(root.find('armModLevel').text), float(root.find('armModRate').text), float(root.find('armAxisX').text), float(root.find('armAxisY').text), float(root.find('armAxisZ').text), int(root.find('armJointNum').text), float(root.find('armRndRate').text)]
	#spineParam
	spineParam = [float(root.find('spineAngleFrom').text), float(root.find('spineAngleTo').text), float(root.find('spineLocXFrom').text), float(root.find('spineLocXTo').text), float(root.find('spineLocYFrom').text), float(root.find('spineLocYTo').text), float(root.find('spineLocZFrom').text), float(root.find('spineLocZTo').text), float(root.find('spinePerc').text), float(root.find('spineArea_limit').text), float(root.find('spineMainLevel').text), int(root.find('spineStepNum').text), float(root.find('spineStepDist').text), float(root.find('spineStepRot').text), float(root.find('spineStartDeg').text), float(root.find('spineStepDeg').text), float(root.find('spineModLevel').text), float(root.find('spineModRate').text), float(root.find('spineAxisX').text), float(root.find('spineAxisY').text), float(root.find('spineAxisZ').text), int(root.find('spineJointNum').text), float(root.find('spineRndRate').text)]
	#spineInsideParam
	spineInsideParam = [float(root.find('spineInsideAngleFrom').text), float(root.find('spineInsideAngleTo').text), float(root.find('spineInsideLocXFrom').text), float(root.find('spineInsideLocXTo').text), float(root.find('spineInsideLocYFrom').text), float(root.find('spineInsideLocYTo').text), float(root.find('spineInsideLocZFrom').text), float(root.find('spineInsideLocZTo').text), float(root.find('spineInsidePerc').text), float(root.find('spineInsideArea_limit').text), float(root.find('spineInsideMainLevel').text), int(root.find('spineInsideStepNum').text), float(root.find('spineInsideStepDist').text), float(root.find('spineInsideStepRot').text), float(root.find('spineInsideStartDeg').text), float(root.find('spineInsideStepDeg').text), float(root.find('spineInsideModLevel').text), float(root.find('spineInsideModRate').text), float(root.find('spineInsideAxisX').text), float(root.find('spineInsideAxisY').text), float(root.find('spineInsideAxisZ').text), int(root.find('spineInsideJointNum').text), float(root.find('spineInsideRndRate').text)]
	#eyeParam
	eyeParam = [float(root.find('eyeAngleFrom').text), float(root.find('eyeAngleTo').text), float(root.find('eyeLocXFrom').text), float(root.find('eyeLocXTo').text), float(root.find('eyeLocYFrom').text), float(root.find('eyeLocYTo').text), float(root.find('eyeLocZFrom').text), float(root.find('eyeLocZTo').text), float(root.find('eyePerc').text),float(root.find('eyeArea_limit').text), int(root.find('eyeType').text), float(root.find('eyeFirstStep').text),  float(root.find('eyeSharpness1').text), float(root.find('eyeSharpness2').text), float(root.find('eyeSharpness3').text), float(root.find('eyeColorR').text), float(root.find('eyeColorG').text), float(root.find('eyeColorB').text), float(root.find('eyeIrisR').text), float(root.find('eyeIrisG').text), float(root.find('eyeIrisB').text)]
	
	#deleteAllObjects()
	#deleteMaterials()

	######### Parameters ########
	makeSeparate = int(basicParam[0])#					makeSeparate - now No use
	bodyColorNum = int(basicParam[1])#					bodyColorNum
	materials = int(headParam[0])#							head01
	baseDist = headParam[1] #									head02
	totalStep = int(headParam[2]) #							head03
	freqA = headParam[3] + mFact[0] #4.0 1.5			head04
	phaseA = headParam[4] + mFact[1] #17.0 3.0	head05
	freqB = headParam[5] + mFact[2] #0.08				head06
	phaseB = headParam[6] + mFact[3] #-0.3			head07
	amplitude = headParam[7] + mFact[4] #				head08
	faceRot = headParam[8] #									head09
	div = int(headParam[9]) # division body				head10
	spinePart = int(headParam[10]) #3						head11
	spineColor = div+1	#			
	spineColorInside = div+2	#	
	legPart = int(headParam[11])	#							head12
	legColor = div+3
	legColorClaw = div+4
	armPart = int(headParam[12]) #							head13
	armColor = div+5
	eyesPart = int(headParam[13]) #							head14
	eyesColor = div+7
	eyeballsColor = div+8
	irisColor = div+9
	bodyColor = div+10
	mouthColor = div+11
	eyeType = int(eyeParam[10]) #0 - 3					eye11
	firstStep = eyeParam[11] #									eye12
	eyeSharpness1 = eyeParam[12] #1.8					eye13
	eyeSharpness2 = eyeParam[13] #0.5					eye14
	eyeSharpness3 = eyeParam[14] #0.5					eye15
	eye_color_r = eyeParam[15] #							eye16
	if eye_color_r == 99.0:
		if random.random() > 0.5:
			eye_color_r = 1.0
		else:
			eye_color_r = 0.0
	eye_color_g = eyeParam[16] #							eye17
	if eye_color_g == 99.0:
		if random.random() > 0.5:
			eye_color_g = 1.0
		else:
			eye_color_g = 0.0
	eye_color_b = eyeParam[17] #							eye18
	if eye_color_b == 99.0:
		if random.random() > 0.5:
			eye_color_b = 1.0
		else:
			eye_color_b = 0.0
	iris_color_r = eyeParam[18] #								eye19
	if iris_color_r == 99.0:
		if random.random() > 0.5:
			iris_color_r = 1.0
		else:
			iris_color_r = 0.0
	iris_color_g = eyeParam[19] #								eye20
	if iris_color_g == 99.0:
		if random.random() > 0.5:
			iris_color_g = 1.0
		else:
			iris_color_g = 0.0
	iris_color_b = eyeParam[20] #								eye21
	if iris_color_b == 99.0:
		if random.random() > 0.5:
			iris_color_b = 1.0
		else:
			iris_color_b = 0.0
	eye_color = [eye_color_r, eye_color_g, eye_color_b] 	#White eye
	iris_color = [iris_color_r, iris_color_g, iris_color_b] 		#Black iris
	###########################

	##### MAKE A HEAD! ########
	addNewPlane() #-------------------------------------------------- First Seed of Object
	#bpy.context.object.rotation_euler[0] = -1.5708
	editMode()
	rot_FaceX(faceRot) # -45	
	objectMode()

	makeMaterials(materials) #--------------------------------------- Make materials for Parts as Legs, etc.

	ob = bpy.context.object
	me = ob.data

	editMode()
	selectAll()
	selectMode2Face()

	oldRatio = 1.0

	########################
	bPhase = bodyParam[0]  + mFact[8] #8.0		body01
	bMod = bodyParam[1] + mFact[9] #20.0			body02
	bAmp = bodyParam[2] + mFact[10] #20.0		body03
	########################
	oneStep = totalStep/div
	for i in range(totalStep):
		mNum = 1
		for j in range(div):
			pNum = j+1
			if i > oneStep * pNum:
				mNum = pNum+1			
		
		objectMode()
		for p in me.polygons:
			if p.select:
				dtRatio = math.sqrt(p.area)
				p.material_index = mNum
		editMode()
		ext_Face(baseDist * dtRatio)
		rotDeg = math.sin(math.radians((i-bPhase) * bMod)) * bAmp
		rot_FaceX(rotDeg)
	
		newRatio = amplitude * math.sin((freqA * i + phaseA) * math.sin(freqB * i + phaseB)) + (amplitude + 1)
		magni = newRatio/oldRatio
		scale_Face(magni)
		oldRatio = newRatio
	ext_Face(baseDist * dtRatio)
	scale_Face(0.2)

	selectAll()
	fillFace()
	deselectAll()

	objectMode()
	bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)
	editMode()

	#makeMouth()

	#No mouth!!!!
	editMode()
	#select p.material_index = 0
	bpy.context.object.active_material_index = 0
	bpy.ops.object.material_slot_select()
	bpy.context.object.active_material_index = 1
	bpy.ops.object.material_slot_assign()

	deselectAll()
	##### MAKE A BODY! ########
	# SELECT ONE FACE #
	# select with the angle, place(x, y) and percent of each faces

	######### Parameters ########
	angleFrom = bodyParam[3] #150.0			body04
	angleTo = bodyParam[4] #180.0					body05
	placeXFrom = bodyParam[5] #					body06
	placeXTo = bodyParam[6] #						body07
	placeYFrom = bodyParam[7] #-24.0			body08
	placeYTo = bodyParam[8] #						body09
	placeZFrom = bodyParam[9] #					body10
	placeZTo = bodyParam[10] #						body11
	perc = bodyParam[11] #								body12
	materialNum = bodyColor
	area_limit = bodyParam[12] #5.0 				body13
	targetMaterial = legPart #2
	###########################

	param = [ angleFrom, angleTo, placeXFrom, placeXTo, placeYFrom, placeYTo, placeZFrom, placeZTo, perc, materialNum, area_limit, targetMaterial ]
	selectFaces(param)

	###########################
	objectMode()
	me = bpy.context.object.data
	for p in me.polygons:
		if p.material_index == bodyColor:
			p.select = True

	for p in me.polygons:
		if p.select == True and p.area > 5000.0:
			p.select = False
			break

	firstBodySize = 0.3
	editMode()
	bpy.context.object.active_material_index = legPart
	bpy.ops.object.material_slot_assign()
	deselectAll()
	bpy.context.object.active_material_index = bodyColor
	bpy.ops.object.material_slot_select()
	bpy.context.object.active_material_index = legPart
	bpy.ops.object.material_slot_assign()
	bpy.ops.mesh.extrude_faces_move(MESH_OT_extrude_faces_indiv={"mirror":False}, TRANSFORM_OT_shrink_fatten={"value":-5.0, "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "release_confirm":False})
	bpy.ops.transform.resize(value=(firstBodySize, firstBodySize, firstBodySize), constraint_axis=(False, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1)
	bpy.ops.mesh.extrude_faces_move(MESH_OT_extrude_faces_indiv={"mirror":False}, TRANSFORM_OT_shrink_fatten={"value":-10.0, "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "release_confirm":False})
	bpy.ops.transform.resize(value=(1.0, 1.0, 1.0), constraint_axis=(False, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1)
	bpy.context.object.active_material_index = bodyColor
	bpy.ops.object.material_slot_assign()
	#bodyShape
	bLength = int(bodyParam[13]) #									body14
	bWidthFreq = bodyParam[14] + mFact[5] #4.0			body15
	bPhase = bodyParam[15] + mFact[6] #-1.0					body16
	bAmp = bodyParam[16] + mFact[7] #							body17
	bDist = bodyParam[17] #												body18
	rFreq = bodyParam[18] #											body19
	rPhase = bodyParam[19] #25.6									body20
	rDeg = bodyParam[20]  #											body21

	me= bpy.context.object.data
	areaP = 0.1
	oldRatio = 1.0
	keisu = 1.0

	for i in range(bLength):
		objectMode()
		for p in me.polygons:
			if p.select == True:
				areaP = p.area
				keisu = math.sqrt(areaP) / 10
				#print(keisu)
			
		dFactor = -keisu * bDist
		newRatio = -math.cos( i * bWidthFreq + bPhase ) + ( bAmp * 2 )
		mFactor = newRatio/oldRatio
		oldRatio = newRatio
		rFactor = math.sin( i * rFreq + rPhase ) * rDeg
		editMode()
	
		bpy.ops.transform.rotate(value=math.radians(rFactor), axis=(-1, -2.22045e-16, 6.61744e-24), constraint_axis=(False, False, False), constraint_orientation='LOCAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1)
		bpy.ops.mesh.extrude_faces_move(MESH_OT_extrude_faces_indiv={"mirror":False}, TRANSFORM_OT_shrink_fatten={"value":dFactor, "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "release_confirm":False})
		bpy.ops.transform.resize(value=(mFactor, mFactor, mFactor), constraint_axis=(False, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1)
	
	########################### Make part samples

	addSubSurf(1)
	applySubSurf()
	delHalf()
	addSubSurf(1)

	#################################################################################### LEG

	# SELECT SOME FACES #
	# select with the angle, place(x, y) and percent of each faces

	######### Parameters ########
	angleFrom = legParam[0] #150.0			legParam01
	angleTo = legParam[1] #180.0				legParam02
	placeXFrom = legParam[2] #					legParam03
	placeXTo = legParam[3] # 						legParam04
	placeYFrom = legParam[4] #-24.0			legParam05
	placeYTo = legParam[5] #						legParam06
	placeZFrom = legParam[6] #					legParam07
	placeZTo = legParam[7] #						legParam08
	perc = legParam[8] #								legParam09
	materialNum = legColor
	area_limit = legParam[9] #5.0					legParam10
	targetMaterial = bodyColor #2
	###########################

	param = [ angleFrom, angleTo, placeXFrom, placeXTo, placeYFrom, placeYTo, placeZFrom, placeZTo, perc, materialNum, area_limit, targetMaterial ]
	selectFaces(param)

	################################################################################### Arm

	# SELECT SOME FACES  #
	# select with the angle, place(x, y) and percent of each faces

	######### Parameters ########
	angleFrom = armParam[0] #								armParam01
	angleTo = armParam[1] #									armParam02
	placeXFrom = armParam[2] #								armParam03
	placeXTo = armParam[3] #									armParam04
	placeYFrom = armParam[4] #								armParam05
	placeYTo = armParam[5] #									armParam06
	placeZFrom = armParam[6] #								armParam07
	placeZTo = armParam[7] #									armParam08
	perc = armParam[8] #											armParam09
	materialNum = armColor
	area_limit = armParam[9] #									armParam10
	targetMaterial = armPart
	###########################

	param = [ angleFrom, angleTo, placeXFrom, placeXTo, placeYFrom, placeYTo, placeZFrom, placeZTo, perc, materialNum, area_limit, targetMaterial ]
	selectFaces(param)

	###########################
	materialNum = legColor 
	mainLevel = legParam[10] #0.3									legParam11
	stepNum = legParam[11] #8 -> 5								legParam12
	stepDist = legParam[12] #-0.2 -> 0.5							legParam13
	stepRot = legParam[13] #-80 -> 10							legParam14
	startDeg = legParam[14] #-30 ->120 90					legParam15
	stepDeg = legParam[15] #-30									legParam16
	modLevel = legParam[16] #0.1 -> -0.3 0.2					legParam17
	modRate = legParam[17]	 #										legParam18
	axis = [legParam[18], legParam[19], legParam[20]] #	legParam19, legParam20, legParam21
	jointNum = legParam[21] #1 -> 2								legParam22
	newMaterialNum = legColorClaw
	rndRate = legParam[22]	#											legParam23
	###########################

	mltData = [ materialNum, mainLevel, stepNum, stepDist, stepRot, startDeg, stepDeg, modLevel, modRate, axis, jointNum, newMaterialNum, rndRate ]
	multiExtrude(mltData)

	#aaa <-- Breaker

	########################### 

	# SELECT FACES BY MATERIAL #

	########################### 
	materialNum = armColor #Arm!
	mainLevel = armParam[10] #												armParam11
	stepNum = armParam[11] #												armParam12
	stepDist = armParam[12]  #												armParam13
	stepRot = armParam[13] #30 											armParam14
	startDeg = armParam[14] #												armParam15
	stepDeg = armParam[15] #												armParam16
	modLevel = armParam[16] #												armParam17
	modRate = armParam[17] #												armParam18
	axis = [armParam[18], armParam[19], armParam[20]] #	armParam19, armParam20, armParam21
	jointNum = armParam[21] #0 											armParam22
	newMaterialNum = armColor
	rndRate = armParam[22] #													armParam23
	###########################

	mltData = [ materialNum, mainLevel, stepNum, stepDist, stepRot, startDeg, stepDeg, modLevel, modRate, axis, jointNum, newMaterialNum, rndRate ]
	multiExtrude(mltData)

	################################################################################## Spine

	# SELECT SOME FACES (LargeSpines) #
	# select with the angle, place(x, y) and percent of each faces

	######### Parameters ########
	angleFrom = spineParam[0] #							spineParam01
	angleTo = spineParam[1] #								spineParam02
	placeXFrom = spineParam[2] #						spineParam03
	placeXTo = spineParam[3] #							spineParam04
	placeYFrom = spineParam[4]  #						spineParam05
	placeYTo = spineParam[5] #							spineParam06
	placeZFrom = spineParam[6] #						spineParam07
	placeZTo = spineParam[7] #							spineParam08
	perc = spineParam[8] #									spineParam09
	materialNum = spineColor
	area_limit = spineParam[9] #							spineParam10
	targetMaterial = spinePart
	###########################

	param = [ angleFrom, angleTo, placeXFrom, placeXTo, placeYFrom, placeYTo, placeZFrom, placeZTo, perc, materialNum, area_limit, targetMaterial ]
	selectFaces(param)

	################################################################################## Spine Inside

	# SELECT SOME FACES (LargeSpines-Inside) #
	# select with the angle, place(x, y) and percent of each faces

	######### Parameters ########
	angleFrom = spineInsideParam[0] #							spineInsideParam01
	angleTo = spineInsideParam[1] #									spineInsideParam02
	placeXFrom = spineInsideParam[2] #							spineInsideParam03
	placeXTo = spineInsideParam[3] #								spineInsideParam04
	placeYFrom = spineInsideParam[4] #							spineInsideParam05
	placeYTo = spineInsideParam[5] #								spineInsideParam06
	placeZFrom = spineInsideParam[6] #							spineInsideParam07
	placeZTo = spineInsideParam[7] #								spineInsideParam08
	perc = spineInsideParam[8] #										spineInsideParam09
	materialNum = spineColorInside
	area_limit = spineInsideParam[9] #								spineInsideParam10
	targetMaterial = spineColorInside
	###########################

	#param = [ angleFrom, angleTo, placeXFrom, placeXTo, placeYFrom, placeYTo, placeZFrom, placeZTo, perc, materialNum, area_limit, targetMaterial ]
	#selectFaces(param)

	###########################
	materialNum = spineColor
	mainLevel = spineParam[10] #												spineParam11
	stepNum = spineParam[11] #													spineParam12
	stepDist = spineParam[12] #													spineParam13 
	stepRot = spineParam[13] #													spineParam14
	startDeg = spineParam[14] #													spineParam15
	stepDeg = spineParam[15] #													spineParam16
	modLevel = spineParam[16] #												spineParam17
	modRate = spineParam[17] #													spineParam18
	axis = [spineParam[18], spineParam[19], spineParam[20]] #	spineParam19, spineParam20, spineParam21
	jointNum = spineParam[21] #													spineParam22
	newMaterialNum = spineColorInside
	rndRate = spineParam[22] #													spineParam23
	###########################

	mltData = [ materialNum, mainLevel, stepNum, stepDist, stepRot, startDeg, stepDeg, modLevel, modRate, axis, jointNum, newMaterialNum, rndRate ]
	multiExtrude(mltData)

	###########################
	materialNum = spineColorInside 
	mainLevel = spineInsideParam[10] #																	spineInsideParam11
	stepNum = spineInsideParam[11] #																	spineInsideParam12
	stepDist = spineInsideParam[12] #																		spineInsideParam13 
	stepRot = spineInsideParam[13]  #																		spineInsideParam14
	startDeg = spineInsideParam[14] #																	spineInsideParam15
	stepDeg = spineInsideParam[15] #																		spineInsideParam16
	modLevel = spineInsideParam[16] #																	spineInsideParam17
	modRate = spineInsideParam[17] #																	spineInsideParam18
	axis = [spineInsideParam[18], spineInsideParam[19], spineInsideParam[20]] #	spineInsideParam19, spineInsideParam20, spineInsideParam21
	jointNum = spineInsideParam[21] #																	spineInsideParam22
	newMaterialNum = spineColorInside
	rndRate = spineInsideParam[22] #																		spineInsideParam23
	############################

	mltData = [ materialNum, mainLevel, stepNum, stepDist, stepRot, startDeg, stepDeg, modLevel, modRate, axis, jointNum, newMaterialNum, rndRate ]
	multiExtrude(mltData)

	################################################################################# Eyes

	# SELECT SOME FACES #
	# select with the angle, place(x, y) and percent of each faces

	######### Parameters ########
	angleFrom = eyeParam[0] #											eyeParam01
	angleTo = eyeParam[1] #												eyeParam02
	placeXFrom = eyeParam[2] #											eyeParam03
	placeXTo = eyeParam[3] #												eyeParam04
	placeYFrom = eyeParam[4] #											eyeParam05
	placeYTo = eyeParam[5] #												eyeParam06
	placeZFrom = eyeParam[6] #											eyeParam07
	placeZTo = eyeParam[7] #												eyeParam08
	perc = eyeParam[8] #														eyeParam09
	materialNum = eyesColor
	area_limit = eyeParam[9] #												eyeParam10
	targetMaterial = eyesPart
	###########################

	param = [ angleFrom, angleTo, placeXFrom, placeXTo, placeYFrom, placeYTo, placeZFrom, placeZTo, perc, materialNum, area_limit, targetMaterial ]
	selectFaces(param)

	eyeInfo = [ eyeballsColor, irisColor, firstStep, eyeSharpness1, eyeSharpness2, eyeSharpness3, materialNum ]
	if eyeType == 0:
		makeBallEyes(eyeInfo)
	elif eyeType == 1:
		makeHoleEyes(eyeInfo)
	elif eyeType == 2:
		makeOhmIrisEyes(eyeInfo)
	elif eyeType == 3:
		makeLongEyes(eyeInfo)

	#################################################################################
	#addSubSurf(1)
	#addMirror()
	objectMode()
	applySubSurf()
	editMode()

	#################################################################################### Claw

	# SELECT SOME FACES #
	# select with the angle, place(x, y) and percent of each faces

	######### Parameters ########
	angleFrom = clawParam[0] #								clawParam01
	angleTo = clawParam[1] #								clawParam02
	placeXFrom = clawParam[2] #							clawParam03
	placeXTo = clawParam[3] #								clawParam04
	placeYFrom = clawParam[4] #						clawParam05
	placeYTo = clawParam[5] #								clawParam06
	placeZFrom = clawParam[6] #						clawParam07
	placeZTo = clawParam[7] #								clawParam08
	perc = clawParam[8] #										clawParam09
	materialNum = legColorClaw
	area_limit = clawParam[9] #								clawParam10
	targetMaterial = legColorClaw
	###########################

	param = [ angleFrom, angleTo, placeXFrom, placeXTo, placeYFrom, placeYTo, placeZFrom, placeZTo, perc, materialNum, area_limit, targetMaterial ]
	selectFaces(param)

	# SELECT FACES BY MATERIAL #

	###########################
	materialNum = legColorClaw #Claw!
	mainLevel = clawParam[10] #												clawParam11
	stepNum = clawParam[11] #												clawParam12
	stepDist = clawParam[12]  #												clawParam13
	stepRot = clawParam[13] #30 											clawParam14
	startDeg = clawParam[14] #												clawParam15
	stepDeg = clawParam[15] #												clawParam16
	modLevel = clawParam[16] #												clawParam17
	modRate = clawParam[17] #												clawParam18
	axis = [clawParam[18], clawParam[19], clawParam[20]] #	clawParam19, clawParam20, clawParam21
	jointNum = clawParam[21] #0 											clawParam22
	newMaterialNum = legColorClaw
	rndRate = clawParam[22] #												clawParam23
	###########################

	mltData = [ materialNum, mainLevel, stepNum, stepDist, stepRot, startDeg, stepDeg, modLevel, modRate, axis, jointNum, newMaterialNum, rndRate ]
	multiExtrude(mltData)

	deselectAll()

	addSubSurf(2)

	#for i in range(materials + 1):
	#	bpy.ops.object.material_slot_remove()

	#set the body color(Color version!)
	objectMode()
	#for p in me.polygons:
	#	if p.material_index != eyeballsColor and p.material_index != legColorSaw and p.material_index != irisColor and p.material_index != 1 and p.material_index != mouthColor:
	#		p.material_index = bodyColor

	#eye_color = [0.0, 0.0, 0.0] #Green eye
	#iris_color = [0.0, 0.0, 0.0] #Black iris
		
	bpy.context.object.active_material_index = eyeballsColor
	bpy.context.object.active_material.diffuse_color = (eye_color)
	bpy.context.object.active_material_index = irisColor
	bpy.context.object.active_material.diffuse_color = (iris_color)
	###applySubSurf()
	if(singleColor):
		randomBodyColor(bodyColorNum, bodyColor, div, materials)
	####addSubSurf(1)
	editMode()
	bpy.context.object.active_material_index = irisColor
	bpy.ops.object.material_slot_select()
	bpy.ops.mesh.looptools_circle(custom_radius=False, fit='best', flatten=True, influence=100, lock_x=False, lock_y=False, lock_z=False, radius=1, regular=True)
	deselectAll()
	objectMode()
		
	addMirror()
	bpy.ops.object.modifier_move_up(modifier="Mirror")
	
	mvLayer = [False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False]
	if(genNum == 0):
		mvLayer[9] = True
	else:
		mvLayer[genNum -1] = True
	bpy.ops.object.move_to_layer(layers=(mvLayer))

#----------------------------------------------------------------- Save .dat file
	fSign = ['_a', '_b', '_c', '_d', '_e', '_f', '_g', '_h']
	titleName = objFile
	if(genNum!=0):
		
		titleName = objFile + fSign[genNum - 1]
		root[0].text = titleName
		root[6].text = str(headParam[3] + mFact[0])
		root[7].text = str(headParam[4] + mFact[1])
		root[8].text = str(headParam[5] + mFact[2])
		root[9].text = str(headParam[6] + mFact[3])
		root[10].text = str(headParam[7] + mFact[4])
		root[31].text = str(bodyParam[14] + mFact[5])	
		root[32].text = str(bodyParam[15] + mFact[6])
		root[33].text = str(bodyParam[16] + mFact[7])
		root[17].text = str(bodyParam[0] + mFact[8])	
		root[18].text = str(bodyParam[1] + mFact[9])
		root[19].text = str(bodyParam[2] + mFact[10])
		tree.write(filePath + titleName + '.dat')
		
	bpy.context.object.name = titleName
		
if __name__ == '__main__':
#	'''

	bpy.types.Scene.singleColor = bpy.props.BoolProperty(name="Single Body Color", description="select singleColor or not", default=False)
	bpy.types.Scene.objName = bpy.props.StringProperty(name="Seed", description="objName", maxlen=1024)
	bpy.types.Scene.numberOfVariation = bpy.props.IntProperty(name="Number Of Variation", default = 0, min=0, max=8, description="set Number of Varie")

#	'''
	register()